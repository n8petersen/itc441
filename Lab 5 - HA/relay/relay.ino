#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Wifi + Broker Info
const char* ssid = "nathan";
const char* password = "ilikepizza123";
const char* server = "192.168.137.2";
const char* ledTopic = "/garage_door";
const char* host = "arduino-relay";  // Create hostname for this device

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

const int relayPin = D1; // pin to toggle relay

unsigned long previousMillis = 0;
long interval = 500; // in ms (milliseconds)

// ## Re-usable function to turn on a specific LED
void toggle_relay() {
  digitalWrite(relayPin, LOW);

  digitalWrite(relayPin, HIGH);
  Serial.println("Relay on");

  // unsigned long currentMillis = millis();
  // if (currentMillis - previousMillis >= interval) {
  //     previousMillis = currentMillis;
  //     digitalWrite(relayPin, LOW);
  //     Serial.println("Relay off");
  // }
  delay(interval);
  digitalWrite(relayPin, LOW);
  Serial.println("Relay off");

}

void setup() {
  Serial.begin(9600);
  Serial.println("");
  Serial.println("Initializing ...");

  // ## Set all color pins to output
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(relayPin, OUTPUT);

  // ## Initialize all LED's to OFF
  digitalWrite(D4, HIGH);  // turn off the built in LED

  // ## Connect to WiFi - Adapted from http://www.esp8266learning.com/wemos-webserver-example.php
  Serial.print("Connecting WiFi to WiFi: ");
  Serial.print(ssid);
  Serial.println("");

  WiFi.hostname(host);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {  //Wait for connection
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi Connected");
  delay(2000);  // Give things time to start up properly

  //  ## Connect to MQTT
  Serial.println("Connecting to MQTT.");
  mqttClient.setServer(server, 1883);
  mqttClient.setCallback(callback);
  mqttClient.connect(host, "mqtt", "mqtt");
  mqttClient.subscribe(ledTopic);
  if (mqttClient.state() == 0) {
    Serial.println("MQTT Connected");
    mqttClient.publish("/debug", "Relay Connected");
  }
  else {
    Serial.print("MQTT was NOT connected, error: ");
    Serial.print(mqttClient.state());
    Serial.println("");
  }

  
  digitalWrite(D4, LOW);  // turn on LED after WiFi and MQTT are connected
}

void loop() {

  mqttClient.loop();


  delay (200);
}

void callback(char* topicChar, byte* payload, unsigned int length) {
  String topic = (String)topicChar;
  String message = "";

  for (int i = 0; i < length; i++) {
    message += (char)payload[i];
  }

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println(message);

  if (topic == (String)ledTopic) {
    if (message == "toggle") {
      toggle_relay();
    }
  }
}